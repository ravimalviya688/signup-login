# README #

## Prerequit for that signup-login api
* Node (Version 10.0.0)
* MySql server should installed
* your favioute text editer(Sublime, Atom etc)


## Important packages that are used to build this Project
### note :- Skelton is generated using express-generator
1. Express Framework and its bydefauld moules.
2. Passport, jwt for authentication 
3. Knex query builder
4. Winston for logging on terminal 
5. bcrypt for password hashing
6. Nodemon for autorestart while save files.

## Some Points that not coverd beacouse of less time.
1. Please make a table "users" with name,email and password column, in your database that can be configured in config/dbconfig.js file.
Note that Table can also be make by migration but it not coverd in this repository.
2. I prefer Mongodb with nodejs but due to system error for mongod i choose mysql.
3. Validations are not perfect. So it should be done properly.


## Development

To develop on the app:

### API

1. Ensure that you have MySql installed and running


2. Install dependencies

```
$ yarn or npm install
```

3. Run the dev server

```
$ npm run dev
```

## Directory Structure


```
▸ bin/               port can be change from here, server running 
▸ config/    database congig file is here
▸ public/
▾ routes/
    index.js
    users.js  contains signup,reset,forget api
▸ uploads/
▸ views/
  app.js       main file , also contain login api
  package.json
  yarn.lock
```



