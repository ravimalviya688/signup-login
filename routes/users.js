var express = require('express');
var bcrypt = require('bcrypt');
var winston = require('winston');
var router = express.Router();

var knex = require('../config/dbconfig.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/signup', function(req, res, next) {
	let name = req.body.name;
	let email = req.body.email;
	let password = req.body.password;

	knex('users').where({
		email:  email,
	})
	.then(response=>{
		if(response && response.length != 0){
			return res.status(400).json('Email already registered.');
		}else{
			bcrypt.hash(password, 10, function(err, password) {
			  if( err ) {
			    winston.warn('bcrypt error', err);
			    return res.status(500).json({ errorMessage: 'Error updating password', error: err });
			  }
			  knex('users').insert({
			  	name: name,
			  	email:  email,
			  	password: password
			  })
			  .then(response=>{
			  	return res.status(201).json('user inserted');
			  })
			  .catch(error=>{
				return res.status(400).json(error);
			  });
			});	
		}
	})
	.catch(error=>{
		return res.status(400).json(error);
	});
});

router.post('/forgetpass', function(req, res, next) {
	let email = req.body.email;

	knex('users').where({
		email:  email,
	})
	.then(response=>{
		if(response && response.length != 0){
			return res.status(201).json('Email found . pLease reset your password now');
		}else{
			return res.status(400).json('Email not Found.');
		}
	})
	.catch(error=>{
		return res.status(201).json(error);
	});
	
});

router.post('/resetpass', function(req, res, next) {
	let newpassword = req.body.newpassword;
	let email = req.body.email;

	knex('users').where({
		email:  email,
	})
	.then(response=>{
		if(response && response.length != 0){
			let data = response[0];
			let pass =  data.password;

			bcrypt.hash(newpassword, 10, function(err, password) {
			  if( err ) {
			    winston.warn('bcrypt error', err);
			    return res.status(500).json({ errorMessage: 'Error updating password', error: err });
			  }
			  knex('users')
			  .where('email', '=', email)
			  .update({
			      password: password
			  })
			  .then(response=>{
			  	return res.status(201).json('password reset successfully');
			  })
			  .catch(error=>{
			  	console.log('error',error);
				return res.status(400).json(error);
			  });
			});	
		}else{
			return res.status(400).json('Email not Found.');
		}
	})
	.catch(error=>{
		console.log('error',error);
		return res.status(400).json(error);
	});
});

router.post('/reset_pass_using_old', function(req, res, next) {
	let newpassword = req.body.newpassword;
	let oldpassword = req.body.oldpassword;
	let email = req.body.email;

	knex('users').where({
		email:  email,
	})
	.then(response=>{
		if(response && response.length != 0){
			let data = response[0];
			let pass =  data.password;
			console.log('pass',pass);
			bcrypt.compare(oldpassword, pass, function(err, result) {
		        if( err || !result ) {
		          return res.status(404).json({ error: 'You entered an Incorrect username or password' });
		        }
		        bcrypt.hash(newpassword, 10, function(err, password) {
		          if( err ) {
		            winston.warn('bcrypt error', err);
		            return res.status(500).json({ errorMessage: 'Error updating password', error: err });
		          }
		          knex('users')
		          .where('email', '=', email)
		          .update({
		              password: password
		          })
		          .then(response=>{
		          	return res.status(201).json('password reset successfully using old password');
		          })
		          .catch(error=>{
		          	console.log('error',error);
		        	return res.status(400).json(error);
		          });
		        });	
		    });
		}else{
			return res.status(400).json('Email not Found.');
		}
	})
	.catch(error=>{
		console.log('error',error);
		return res.status(400).json(error);
	});
});


module.exports = router;
