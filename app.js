var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var winston = require('winston');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var jwt = require('jsonwebtoken');
const passport = require('passport');
const passportJWT = require("passport-jwt");


const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'chanukah-tibiae-wave-penman';

const strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {

  knex('users').where({
    id:  jwt_payload.user_id,
  })
  .then(response=>{
    if(response && response.length != 0){
      next(null, doc);
    }else{
      next(null, false);
    }
  })
  .catch(error=>{
    console.log('error',error);
    return res.status(400).json(error);
  });
});

passport.use(strategy);

var knex = require('./config/dbconfig.js');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// routes
app.use('/', indexRouter);
app.post('/users/login', function(req, res, next){
  let email = req.body.email;
  knex('users').where({
    email:  email,
  })
  .then(response=>{
    if(response && response.length != 0){
      let data = response[0];
      let pass =  data.password;
      console.log('pass',pass);
      bcrypt.compare(req.body.password, pass, function(err, result) {
          if( err || !result ) {
            return res.status(404).json({ error: 'You entered an Incorrect username or password' });
          }
          const secretKey = 'chanukah-tibiae-wave-penman';
          const payload = { user_id: data.id };
          const token = jwt.sign(payload, secretKey);

          return res.status(200).json({ token: token, data : data});
      });
    }else{
      return res.status(400).json('User not Found.');
    }
  })
  .catch(error=>{
    console.log('error',error);
    return res.status(400).json(error);
  });
});
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
